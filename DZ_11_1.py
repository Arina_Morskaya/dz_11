import sys


def decorator(func):
    def wrapper(*args, **kwargs):
        try:
            return func(*args, **kwargs)
        except Exception:
            exc_type, exc_value, tb = sys.exc_info()
            return 'Exception - {}'.format(exc_type)
    return wrapper


@decorator
def generator_error():
    raise IndexError


test = generator_error()
print(test)
