from contextlib import suppress


def suppresses_errors(*argument):
    def decorator(func):
        def wrapper(*args, **kwargs):
            with suppress(argument):
                result = func(*args, **kwargs)
                return result
        return wrapper
    return decorator


@suppresses_errors(NameError, ZeroDivisionError)
def generator_error():
    print(1 / 0)
    raise NameError


test = generator_error()
print(test)
