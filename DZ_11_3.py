import sys
import requests
import json
import time


class MyContextManager(object):
    def __init__(self):
        self.start = time.time()

    def __enter__(self):
        print('ContextManager:')

    def __exit__(self, exc_type, exc_val, exc_tb):
        print('Request execution time - ', time.time() - self.start)


def get_cargo_types():
    api = "https://api.novaposhta.ua/v2.0/json/"
    data = {
        "modelName": "Common",
        "calledMethod": "getCargoTypes",
        "methodProperties": {},
        "apiKey": "c10df27d575446c2d010a2d14b544648"
    }
    r = requests.post(url=api, json=data).json()
    with open('D:/CargoTypes.txt', 'w') as response:
        json.dump(r, response, indent=4, ensure_ascii=False)
    return


with MyContextManager():
    get_cargo_types()
